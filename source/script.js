(() => {
  const DATA = [
    {
      title: 'DOTA 2',
      text:
        'Dota 2 is a multiplayer online multiplayer online battle team arena team game developed by Valve Corporation. The game is a continuation of DotA - a custom modification card for Warcraft III: Reign of Chaos and Warcraft III: The Frozen Throne additions. The game depicts a battle on a special kind of map; Each game involves two teams of five players controlling the “heroes” - characters with different sets of abilities. To win the match, the team must destroy the special “fortress” object belonging to the enemy side and protect its own “fortress” from destruction. Dota 2 works on a free-to-play model with elements of micropayments.',
      icon: 'assets/icons/dota.png',
      image: 'assets/images/dota.png',
      like: false
    },
    {
      title: 'LOL',
      text:
        'League of Legends (translated from English - "League of Legends"), abbreviated LoL - role-playing video game with elements of real-time strategy (MOBA / Action RTTS), developed and released by Riot Games on October 27, 2009 for Microsoft Windows and Apple platforms Macintosh [1]. The game is distributed according to the free-to-play model. The monthly audience of the game is 100 million players worldwide.',
      icon: 'assets/icons/lol.png',
      image: 'assets/images/lol.png',
      like: false
    },
    {
      title: 'StarCraft II',
      text:
        'StarCraft II: Wings of Liberty (from the English - "Wings of Liberty") - a computer game in the genre of real-time strategy, a continuation of StarCraft. It was announced on May 19, 2007 at the Blizzard Worldwide Invitational Festival in Seoul, South Korea. Unlike the previous game of the series, StarCraft II is made in three-dimensional graphics, and also uses the physics engine Havok. One game was originally conceived, but at BlizzCon 2008 it was announced that StarCraft II would become a trilogy. The first part was called StarCraft II: Wings of Liberty, and two additions - Heart of the Swarm and Legacy of the Void. StarCraft II: Wings of Liberty was released on July 27, 2010.',
      icon: 'assets/icons/starcraft.png',
      image: 'assets/images/starcraft.png',
      like: false
    },
    {
      title: 'CS GO',
      text:
        'Counter-Strike: Global Offensive (CS: GO; from English - "Counter-Strike: Global Offensive") is a multiplayer computer game developed by Valve and Hidden Path Entertainment. The latest major game in the Counter-Strike series; Like all games in the series, it is dedicated to confronting terrorists and special forces.',
      icon: 'assets/icons/cs_go.png',
      image: 'assets/images/cs_go.png',
      like: false
    }
  ];

  const NODES = {
    menu: document.querySelector('.menu'),
    menuList: document.getElementById('menu-list'),
    submenu: document.querySelector('.submenu'),
    submenuHeader: document.getElementById('submenu-header'),
    submenuWallpaper: document.getElementById('submenu-wallpaper'),
    submenuAbout: document.getElementById('submenu-about'),
    about: document.querySelector('.about'),
    aboutHeader: document.getElementById('about-header'),
    aboutText: document.getElementById('about-text'),
    wallpaper: document.querySelector('.wallpaper'),
    wallpaperHeader: document.getElementById('wallpaper-header'),
    like: document.getElementById('like'),
    download: document.getElementById('download')
  };

  let active_item = 0;
  let active_page = 0;

  // отображение экрана
  function set_page(page) {
    NODES.menu.style.display = 'none';
    NODES.submenu.style.display = 'none';
    NODES.about.style.display = 'none';
    NODES.wallpaper.style.display = 'none';
    switch (page) {
      case 0:
        NODES.menu.style.display = '';
        break;
      case 1:
        NODES.submenu.style.display = '';
        break;
      case 2:
        NODES.about.style.display = '';
        break;
      case 3:
        NODES.wallpaper.style.display = '';
        break;
    }
  }

  function go_back() {
    set_page(--active_page);
  }

  function goto_wallpaper() {
    active_page = 3;
    set_page(active_page);
  }

  function goto_about() {
    active_page = 2;
    set_page(active_page);
  }

  function goto_submenu() {
    active_page = 1;
    set_page(active_page);
  }

  // установка активного айтема
  function set_item(e) {
    let target = e.target;
    while (!target.classList.contains('list-item')) target = target.parentNode;
    active_item = +target.id.slice(-1);
    set_content();
    set_page(++active_page);
  }

  function set_like() {
    if (DATA[active_item].like) NODES.like.children[0].classList.add('active');
    else NODES.like.children[0].classList.remove('active');
  }

  function toggle_like() {
    DATA[active_item].like = !DATA[active_item].like;
    set_like();
  }

  function set_content() {
    NODES.submenuHeader.children[1].src = DATA[active_item].icon;
    NODES.submenuHeader.children[2].textContent = DATA[active_item].title;
    NODES.aboutHeader.children[1].src = DATA[active_item].icon;
    NODES.aboutText.textContent = DATA[active_item].text;
    NODES.wallpaper.style.backgroundImage = `url("${DATA[active_item].image}")`;
    NODES.wallpaperHeader.children[1].src = DATA[active_item].icon;
    set_like();
    NODES.download.href = DATA[active_item].image;
  }

  // начальная инициализация
  function init() {
    // установка первого экрана меню
    set_page(active_page);
    // установка контента по всем экранам
    set_content();
    // установка листнеров на меню
    for (let i = 0; i < NODES.menuList.children.length; i++) {
      NODES.menuList.children[i].addEventListener('click', set_item);
    }
    // установка листнеров на сабменю
    NODES.submenuHeader.addEventListener('click', go_back);
    NODES.submenuWallpaper.addEventListener('click', goto_wallpaper);
    NODES.submenuAbout.addEventListener('click', goto_about);
    // установка листнеров для about
    NODES.aboutHeader.addEventListener('click', goto_submenu);
    // установка листнеров для обоев
    NODES.wallpaperHeader.addEventListener('click', goto_submenu);
    NODES.like.addEventListener('click', toggle_like);
  }

  init();
})();
